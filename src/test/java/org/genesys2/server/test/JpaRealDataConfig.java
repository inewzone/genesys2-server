/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.genesys2.spring.hazelcast.HazelcastCacheRegionFactoryWrapper;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Ignore
@Configuration
@PropertySource("classpath:/spring/spring-database.properties")
@EnableJpaRepositories(basePackages = { "org.genesys2.server.persistence.acl", "org.genesys2.server.persistence.domain" }, repositoryImplementationPostfix = "CustomImpl", entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager")
@EnableTransactionManagement
@TransactionConfiguration(defaultRollback = true, transactionManager = "transactionManager")
public class JpaRealDataConfig {
	@Autowired
	private Environment env;

	@Bean(name = "dataSource", destroyMethod = "close")
	public DataSource dataSource() throws Exception {
		final DataSource dataSource = new DataSource();

		dataSource.setDriverClassName(env.getProperty("db.driverClassName"));
		dataSource.setUrl(env.getProperty("db.url"));
		dataSource.setUsername(env.getProperty("db.username"));
		dataSource.setPassword(env.getProperty("db.password"));
		dataSource.setValidationQuery("SELECT 1");
		dataSource.setTestWhileIdle(true);
		dataSource.setTestOnBorrow(true);
		dataSource.setTestOnConnect(true);
		dataSource.setValidationInterval(60);
		dataSource.setMinIdle(3);
		dataSource.setMaxActive(10);
		dataSource.setMaxIdle(5);
		dataSource.setInitialSize(5);
		
		return dataSource;
	}

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(HazelcastCacheRegionFactoryWrapper wrp) throws Exception {
		final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();

		bean.setDataSource(dataSource());
		bean.setPersistenceUnitName("spring-jpa");
		bean.setPackagesToScan("org.genesys2.server.model");
		bean.setPersistenceProvider(new HibernatePersistenceProvider());

		final HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(env.getProperty("db.showSql", Boolean.TYPE, true));
		jpaVendorAdapter.setGenerateDdl(env.getProperty("db.hbm2ddl", Boolean.TYPE, true));
		Properties jpaProperties = jpaProperties();
		// for (Object key : jpaProperties.keySet()) {
		// System.err.println("JPA: " + key + " = " + jpaProperties.get(key));
		// }
		bean.setJpaProperties(jpaProperties);

		bean.setJpaVendorAdapter(jpaVendorAdapter);

		return bean;
	}

	@Bean
	public PersistenceExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}

	@Bean(name = "transactionManager")
	public JpaTransactionManager jpaTransactionManager(EntityManagerFactory entityManagerFactory) throws Exception {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setDataSource(dataSource());
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

	@Bean(name = "jpaProperties")
	public Properties jpaProperties() throws Exception {
		Properties jpaProp = new Properties();
		jpaProp.load(getClass().getResourceAsStream("/spring/hibernate-mysql.properties"));
		return jpaProp;
	}
}
