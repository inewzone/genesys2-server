/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import org.apache.velocity.app.VelocityEngine;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.persistence.domain.*;
import org.genesys2.server.service.*;
import org.genesys2.server.service.impl.*;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AbstractServicesTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public abstract class AbstractServicesTest {

	@Configuration
	@Import({ JpaDataConfig.class, HazelcastConfig.class })
	@PropertySource({ "classpath:application.properties", "classpath:spring/spring.properties" })
	public static class Config {

		@Bean
		public UserDetailsService userDetailsService() {
			return new AuthUserDetailsService();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public ContentSanitizer contentSanitizer() {
			return new ContentSanitizer();
		}

		@Bean
		public VelocityEngine velocityEngine() {
			return new VelocityEngine();
		}

		@Bean
		public DescriptorService descriptorService() {
			return new DescriptorServiceImpl();
		}

		@Bean
		public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
			return new ThreadPoolTaskExecutor();
		}

		@Bean
		public EMailService eMailService() {
			return new EMailServiceImpl();
		}

		@Bean
		public JavaMailSender mailSender() {
			return Mockito.mock(JavaMailSenderImpl.class);
		}

		@Bean
		public EMailVerificationService emailVerificationService() {
			return new EMailVerificationServiceImpl();
		}

		@Bean
		public TokenVerificationService tokenVerificationService() {
			return new TokenVerificationServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public TeamService teamService() {
			return new TeamServiceImpl();
		}

        @Bean
        public GenesysLowlevelRepository genesysLowlevelRepositoryCustomImpl() {
            return new GenesysLowlevelRepositoryCustomImpl();
        }


        @Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public GenesysFilterService genesysFilterService() {
			return new GenesysFilterServiceImpl();
		}

		@Bean
		public MappingService mappingService() {
			return new MappingServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public TraitValueRepository traitValueRepository() {
			return new TraitValueRepositoryImpl();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public TraitService traitService() {
			return new TraitServiceImpl();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}
	}

	@Autowired
	public UserDetailsService userDetailsService;

	@Autowired
	public UserPersistence userPersistence;

	@Autowired
	public ContentSanitizer contentSanitizer;

	@Autowired
	public ActivityPostRepository postRepository;

	@Autowired
	public DescriptorService descriptorService;

	@Autowired
	public DescriptorRepository descriptorRepository;

	@Autowired
	public JavaMailSender mailSender;

	@Autowired
	public EMailService eMailService;

	@Autowired
	public VerificationTokenRepository verificationTokenRepository;

	@Autowired
	public EMailVerificationService emailVerificationService;

	@Autowired
	public ArticleRepository articleRepository;

	@Autowired
	public HtmlSanitizer htmlSanitizer;

	@Autowired
	public TaxonomyService taxonomyService;

	@Autowired
	public TokenVerificationService tokenVerificationService;

	@Autowired
	public VerificationTokenRepository tokenRepository;

	@Autowired
	public InstituteService instituteService;

	@Autowired
	public TeamRepository teamRepository;

	@Autowired
	public TeamService teamService;

	@Autowired
	public UserService userService;

	@Autowired
	public CropService cropService;

	@Autowired
	public CropRepository cropRepository;

	@Autowired
	public CropRuleRepository cropRuleRepository;

	@Autowired
	public CropTaxonomyRepository cropTaxonomyRepository;

	@Autowired
	public Taxonomy2Repository taxonomy2Repository;

	@Autowired
	public GenesysFilterService genesysFilterService;

	@Autowired
	public MappingService mappingService;
}
