#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=Não autorizado
http-error.401.text=Autenticação obrigatória
http-error.403=Acesso negado
http-error.403.text=Você não tem permissão para acessar o recurso
http-error.404=Não encontrado
http-error.404.text=O recurso solicitado não foi encontrado, mas pode vir a ser disponibilizado no futuro.
http-error.500=Erro interno do servidor
http-error.500.text=Encontramos um erro e não há outras mensagens relevantes.
http-error.503=Serviço não disponível
http-error.503.text=O servidor encontra-se indisponível (por sobrecarga ou manutenção.)


# Login
login.username=Nome de usuário
login.password=Senha
login.invalid-credentials=Credenciais inválidas.
login.remember-me=Lembrar
login.login-button=Entrar
login.register-now=Criar uma conta
logout=Sair
login.forgot-password=Esqueci minha senha
login.with-google-plus=Entrar com Google+

# Registration
registration.page.title=Criar uma conta de usuário
registration.title=Criar sua conta
registration.invalid-credentials=Credenciais inválidas.
registration.user-exists=Nome de usuário já existente.
registration.email=Email
registration.password=Senha
registration.confirm-password=Repetir senha
registration.full-name=Nome completo
registration.create-account=Criar conta
captcha.text=Texto de captcha


id=ID

name=Nome
description=Descrição
actions=Ações
add=Adicionar
edit=Editar
save=Salvar
create=Criar
cancel=Cancelar
delete=Excluir

jump-to-top=Voltar ao início\!

pagination.next-page=Próximo >
pagination.previous-page=< Anterior


# Language
locale.language.change=Alterar local
i18n.content-not-translated=Este conteúdo não está disponível no seu idioma. Entre em contato caso possa ajudar na tradução.

data.error.404=Os dados solicitados não foram encontrados no sistema.
page.rendertime=O processamento desta página demorou {0}min.

footer.copyright-statement=© Fornecedores de Dados de 2013 e GCDT

menu.home=Início
menu.browse=Navegar
menu.datasets=Dados C&E
menu.descriptors=Descritores
menu.countries=Países
menu.institutes=Institutos
menu.my-list=Minha lista
menu.about=Sobre
menu.contact=Contato
menu.disclaimer=Aviso de isenção
menu.feedback=Comentários
menu.help=Ajuda
menu.terms=Termos e Condições de Uso
menu.copying=Política de direitos autorais
menu.privacy=Política de privacidade

page.home.title=PGR do Genesys

user.pulldown.administration=Administração
user.pulldown.users=Lista de usuários
user.pulldown.logout=Sair
user.pulldown.profile=Ver perfil
user.pulldown.oauth-clients=Clientes OAuth
user.pulldown.teams=Equipes

user.pulldown.heading={0}
user.create-new-account=Criar uma conta
user.full-name=Nome completo
user.email=Endereço de email
user.account-status=Status da conta
user.account-disabled=Conta desabilitada
user.account-locked-until=Conta bloqueada até
user.roles=Funções dos usuários
userprofile.page.title=Perfil do usuário
userprofile.update.title=Atualize seu perfil

user.page.list.title=Contas de usuário registradas

crop.croplist=Lista de lavouras
crop.all-crops=Todas as lavouras
crop.page.profile.title={0} perfil
crop.taxonomy-rules=Regras taxonômicas
crop.view-descriptors=Ver descritores de culturas...

activity.recent-activity=Atividade recente

country.page.profile.title=Perfil do país\: {0}
country.page.list.title=Lista de países
country.page.not-current=Esta é uma entrada do histórico.
country.page.faoInstitutes={0} institutos registrados no WIEWS
country.stat.countByLocation={0} acessões em institutos neste país
country.stat.countByOrigin={0} acessões registadas no Genesys vêm deste país.
country.statistics=Estatísticas do país
country.accessions.from=Acessões coletadas em {0}
country.more-information=Mais informações\:
country.replaced-by=Código de país é substituído por\: {0}
country.is-itpgrfa-contractingParty={0} é parte no Tratado Internacional sobre os Recursos Fitogenéticos para Alimentação e Agricultura (ITPGRFA).
select-country=Selecionar país

faoInstitutes.page.list.title=Institutos WIEWS
faoInstitutes.page.profile.title=WIEWS {0}
faoInstitutes.stat.accessionCount=Acessões no Genesys\: {0}.
faoInstitutes.stat.datasetCount=Conjuntos adicionais de dados no Genesys\: {0}.
faoInstitute.stat-by-crop=Lavouras mais representadas
faoInstitute.stat-by-genus=Gêneros mais representados
faoInstitute.stat-by-species=Espécies mais representadas
faoInstitute.accessionCount={0} acessões
faoInstitute.statistics=Estatísticas do instituto
faoInstitutes.page.data.title=Acessões em {0}
faoInstitute.accessions.at=Acessões em {0}
faoInstitutes.viewAll=Ver todos os institutos registrados
faoInstitutes.viewActiveOnly=Ver institutos com acessões no Genesys
faoInstitute.no-accessions-registered=Entre em contacto conosco caso possa facilitar o fornecimento de dados deste instituto.
faoInstitute.institute-not-current=Este registro está arquivado.
faoInstitute.view-current-institute=Navegar até {0}.
faoInstitute.country=País
faoInstitute.code=Código WIEWS
faoInstitute.email=Email de contato
faoInstitute.acronym=Acrônimo
faoInstitute.url=Link da web
faoInstitute.member-of-organizations-and-networks=Organizações e Redes\:
faoInstitute.uniqueAcceNumbs.true=Cada número de acessão é único neste instituto
faoInstitute.uniqueAcceNumbs.false=O mesmo número de acessão pode ser usado em diferentes coletas neste instituto.
faoInstitute.requests.mailto=Endereço de email para requisição de materiais\:
faoInstitute.allow.requests=Permitir requisição de material
faoInstitute.sgsv=Código SGSV

view.accessions=Acessões a partir de navegações
view.datasets=Exibir conjunto de dados
paged.pageOfPages=Página {0} de {1}
paged.totalElements={0} entradas

accessions.number={0} acessões
accession.metadatas=Dados C&E
accession.methods=Caracterização & Dados de avaliação
unit-of-measure=Unidade de medida

ce.trait=Característica
ce.sameAs=O mesmo que
ce.methods=Métodos
ce.method=Método
method.fieldName=Campo DB

accession.uuid=UUID
accession.accessionName=Número da acessão
accession.origin=País de origem
accession.holdingInstitute=Instituto depositário
accession.holdingCountry=Localização
accession.taxonomy=Nome científico
accession.crop=Nome da lavoura
accession.otherNames=Também conhecido como
accession.inTrust=Em Confiança
accession.mlsStatus=Status MLS
accession.duplSite=Instituto de duplicação segura
accession.inSvalbard=Duplicado com segurança no Svalbard
accession.inTrust.true=Esta acessão obedece ao Artigo 15 do Tratado Internacional de Recursos Genéticos Vegetais para Alimentação e Agricultura.
accession.mlsStatus.true=Esta acessão pertence ao Sistema Multilateral do ITPGRFA.
accession.inSvalbard.true=Duplicado com segurança no Svalbard Global Seed Vault.
accession.not-available-for-distribution=A acessão NÃO está disponível para distribuição.
accession.available-for-distribution=A acessão está disponível para distribuição.
accession.elevation=Elevação
accession.geolocation=Geolocalização (lat, long)

accession.storage=Tipo de estoque de Germplasm
accession.storage.=
accession.storage.10=Coleta de sementes
accession.storage.11=Curto-prazo
accession.storage.12=Médio-prazo
accession.storage.13=Longo-prazo
accession.storage.20=Coleta de campo
accession.storage.30=Coleta in vitro
accession.storage.40=Coleta criopreservada
accession.storage.50=Coleta de DNA
accession.storage.99=Outros

accession.breeding=Informação do melhorista
accession.breederCode=Código do melhorista
accession.pedigree=Linhagem
accession.collecting=Informações da coleta
accession.collecting.site=Localização do ponto de coleta
accession.collecting.institute=Instituto de coleta
accession.collecting.number=Número da coleta
accession.collecting.date=Data de coleta da amostra
accession.collecting.mission=ID da missão de coleta
accession.collecting.source=Fonte da coleta/aquisição

accession.collectingSource.=
accession.collectingSource.10=Habitat natural
accession.collectingSource.11=Floresta ou cerrado
accession.collectingSource.12=Capoeira
accession.collectingSource.13=Campo
accession.collectingSource.14=Deserto ou tundra
accession.collectingSource.15=Habitat aquático
accession.collectingSource.20=Campo ou habitat cultivado
accession.collectingSource.21=Campo
accession.collectingSource.22=Pomar
accession.collectingSource.23=Quintal, cozinha ou jardim (urbano, periferia ou rural)
accession.collectingSource.24=Pousio
accession.collectingSource.25=Pastagem
accession.collectingSource.26=Silo
accession.collectingSource.27=Área de debulhagem
accession.collectingSource.28=Parque
accession.collectingSource.30=Mercado ou loja
accession.collectingSource.40=Instituto, estação experimental, organização de pesquisa, banco de genes
accession.collectingSource.50=Empresa de sementes
accession.collectingSource.60=Habitat de ervas daninhas, danificado ou ruderal
accession.collectingSource.61=Beira-de-estrada
accession.collectingSource.62=Margem de campo
accession.collectingSource.99=Outros

accession.donor.institute=Instituto do doador
accession.donor.accessionNumber=ID da acessão do doador
accession.geo=Informação geográfica
accession.geo.datum=Datum de coordenadas
accession.geo.method=Método georeferencial
accession.geo.uncertainty=Incerteza das coordenadas
accession.sampleStatus=Status biológico da acessão

accession.sampleStatus.=
accession.sampleStatus.100=Selvagem
accession.sampleStatus.110=Natural
accession.sampleStatus.120=Semi-natural/selvagem
accession.sampleStatus.130=Semi-natural/semeada
accession.sampleStatus.200=Daninha
accession.sampleStatus.300=Variedade primitiva/cultivar tradicional
accession.sampleStatus.400=Material para reprodução/pesquisa
accession.sampleStatus.410=Linhagem do reprodutor
accession.sampleStatus.411=População sintética
accession.sampleStatus.412=Híbrido
accession.sampleStatus.413=Estoque inicial/população base
accession.sampleStatus.414=Linhagem consanguínea
accession.sampleStatus.415=População de segregação
accession.sampleStatus.416=Seleção clonal
accession.sampleStatus.420=Estoque genético
accession.sampleStatus.421=Mutante
accession.sampleStatus.422=Estoques citogenéticos
accession.sampleStatus.423=Outros estoques genéticos
accession.sampleStatus.500=Cultivar avançado/melhorado
accession.sampleStatus.600=OGM
accession.sampleStatus.999=Outros

accession.availability=Disponibilidade para distribuição
accession.aliasType.ACCENAME=Nome de acessão
accession.aliasType.DONORNUMB=Identificador da acessão do doador
accession.aliasType.BREDNUMB=Nome designado pelo melhorista
accession.aliasType.COLLNUMB=Número de coleta
accession.aliasType.OTHERNUMB=Outros nomes
accession.aliasType.DATABASEID=(ID da base de dados)
accession.aliasType.LOCALNAME=(Nome local)

accession.availability.=Desconhecido
accession.availability.true=Disponível
accession.availability.false=Não disponível

accession.page.profile.title=Perfil de acessão\: {0}
accession.page.resolve.title=Múltiplas acessões encontradas
accession.resolve=Múltiplas acessões com o nome "{0}" encontradas no Genesys. Selecione uma da lista.
accession.page.data.title=Pesquisador de acessões
accession.taxonomy-at-institute=Ver {0} a {1}

accession.svalbard-data=Dados de duplicação do Banco Global de Sementes de Svalbard
accession.svalbard-data.taxonomy=Nome científico informado
accession.svalbard-data.depositDate=Data de depósito
accession.svalbard-data.boxNumber=Número da caixa
accession.svalbard-data.quantity=Quantidade
accession.remarks=Comentários

taxonomy.genus=Gênero
taxonomy.species=Espécie
taxonomy.taxonName=Nome científico
taxonomy-list=Lista de taxonomias

selection.page.title=Acessões selecionadas
selection.add=Adicionar {0} à lista
selection.remove=Remover {0} da lista
selection.clear=Limpar a lista
selection.empty-list-warning=Você não adicionou nenhuma acessão à lista.
selection.add-many=Verificar e adicionar
selection.add-many.accessionIds=Listar IDs de acessões conforme usados no Genesys, separados por espaço ou nova linha.

savedmaps=Lembrar mapa atual
savedmaps.list=Lista de mapa
savedmaps.save=Lembrar mapa

filter.enter.title=Digite o título do filtro
filters.page.title=Filtros de dados
filters.view=Filtros atuais
filter.filters-applied=Você usou filtros.
filter.filters-not-applied=Você pode filtrar os dados.
filters.data-is-filtered=Os dados estão filtrados.
filters.toggle-filters=Filtros
filter.taxonomy=Nome científico
filter.art15=Acessão ITPGRFA Art. 15
filter.acceNumb=Número da acessão
filter.alias=Nome da acessão
filter.crops=Nome da lavoura
filter.orgCty.iso3=País de origem
filter.institute.code=Nome do instituto depositário
filter.institute.country.iso3=País do instituto depositário
filter.sampStat=Status biológico da acessão
filter.institute.code=Nome do instituto depositário
filter.geo.latitude=Latitude
filter.geo.longitude=Longitude
filter.geo.elevation=Elevação
filter.taxonomy.genus=Genus
filter.taxonomy.species=Espécie
filter.taxSpecies=Espécie
filter.taxonomy.sciName=Nome científico
filter.sgsv=Duplicado com segurança no Svalbard
filter.mlsStatus=Status MLS da acessão
filter.available=Disponível para distribuição
filter.donorCode=Instituto do doador
filter.duplSite=Local da duplicação segura
filter.download-dwca=Fazer o download de ZIP
filter.add=Adicionar filtro
filter.additional=Filtros adicionais
filter.apply=Aplicar
filter.close=Fechar
filter.remove=Remover filtro
filter.autocomplete-placeholder=Digite 3 caracteres ou mais...
filter.coll.collMissId=ID da missão de coleta
filter.storage=Tipo de armazenagem de Germplasm
filter.string.equals=Equivale a
filter.string.like=Começa com


search.page.title=Pesquisa de texto completo
search.no-results=Não foram encontrados resultados para a sua pesquisa.
search.input.placeholder=Pesquisar no Genesys...
search.search-query-missing=Digite sua pesquisa.
search.search-query-failed=Erro na pesquisa {0}
search.button.label=Pesquisar

admin.page.title=Administração do Genesys 2
metadata.page.title=Conjuntos de dados
metadata.page.view.title=Detalhes do conjunto de dados
metadata.download-dwca=Download ZIP
page.login=Entrar

traits.page.title=Descritores
trait-list=Descritores


organization.page.list.title=Organizações e Redes
organization.page.profile.title={0}
organization.slug=Acrônimo da organização ou rede
organization.title=Nome completo
filter.institute.networks=Rede


menu.report-an-issue=Relatar um problema
menu.scm=Código fonte
menu.translate=Traduzir o Genesys

article.edit-article=Editar artigo
article.slug=Slug do artigo (URL)
article.title=Título do artigo
article.body=Corpo do artigo

activitypost=Post de atividade
activitypost.add-new-post=Adicionar novo post
activitypost.post-title=Título do post
activitypost.post-body=Corpo

blurp.admin-no-blurp-here=Não há blurp aqui.
blurp.blurp-title=Título do blurp
blurp.blurp-body=Conteúdo do blurp
blurp.update-blurp=Salvar blurp


oauth2.confirm-request=Confirmar acesso
oauth2.confirm-client=Você, <b>{0}</b>, por este meio autoriza <b>{1}</b> a aceder aos seus recursos protegidos.
oauth2.button-approve=Sim, permitir acesso
oauth2.button-deny=Não, negar acesso

oauth2.authorization-code=Código de autorização
oauth2.authorization-code-instructions=Copie este código de autorização\:

oauth2.access-denied=Acesso negado
oauth2.access-denied-text=Você negou acesso aos seus recursos.

oauth-client.page.list.title=Clientes OAuth2
oauth-client.page.profile.title=Cliente OAuth2\: {0}
oauth-client.active-tokens=Lista de tokens emitidos
client.details.title=Título do cliente
client.details.description=Descrição

maps.loading-map=Carregando mapa...
maps.view-map=Ver mapa
maps.accession-map=Mapa de acessão

audit.createdBy=Criado por {0}
audit.lastModifiedBy=Última atualização feita por {0}

itpgrfa.page.list.title=Partes no ITPGRFA

request.page.title=Solicitando material aos institutos depositários
request.total-vs-available=De {0} acessões listadas, sabe-se que {1} estão disponíveis para distribuição.
request.start-request=Solicitar germoplasma disponível
request.your-email=Seu endereço de email\:
request.accept-smta=Confirmação de SMTA/MTA
request.smta-will-accept=Aceito os termos e condições SMTA/MTA
request.smta-will-not-accept=Não aceito os termos e condições SMTA/MTA
request.smta-not-accepted=Você não indicou que aceita os termos e condições do SMTA/MTA. Sua requisição de material não será processada.
request.purpose=Especifique o uso de material\:
request.purpose.0=Outros (utilize o campo Notas)
request.purpose.1=Pesquisa de alimentos e agricultura
request.notes=Forneça comentários adicionais e notas\:
request.validate-request=Validar sua solitação
request.confirm-request=Confirmar recebimento do pedido
request.validation-key=Chave de validação\:
request.button-validate=Validar
validate.no-such-key=Chave de validação inválida.

team.user-teams=Equipes do usuário
team.create-new-team=Criar nova equipe
team.team-name=Nome da equipe
team.leave-team=Sair da equipe
team.team-members=Membros da equipe
team.page.profile.title=Equipe\: {0}
team.page.list.title=Todas as equipes
validate.email.key=Digite a chave
validate.email=Validação de email
validate.email.invalid.key=Chave inválida

edit-acl=Editar permissões
acl.page.permission-manager=Gerente de permissões
acl.sid=Identidade de segurança
acl.owner=Proprietário do objeto
acl.permission.1=Ler
acl.permission.2=Escrever
acl.permission.4=Criar
acl.permission.8=Excluir
acl.permission.16=Gerenciar


ga.tracker-code=Código de rastreamento GA

boolean.true=Sim
boolean.false=Não
boolean.null=Desconhecido
userprofile.password=Redefinir a senha
userprofile.enter.email=Digite seu email
userprofile.enter.password=Digite uma nova senha
userprofile.email.send=Enviar email

verification.invalid-key=Chave token inválida.
verification.token-key=Chave de validação
login.invalid-token=Token de acesso inválido

descriptor.category=Categoria do descritor
method.coding-table=Tabela de códigos

oauth-client.info=Informações do cliente
oauth-client.list=Lista de clientes oauth
client.details.client.id=ID de detalhes do cliente
client.details.additional.info=Informações adicionais
client.details.token.list=Lista de tokens
client.details.refresh-token.list=Lista de atualização de tokens
oauth-client.remove=Excluir
oauth-client.remove.all=Excluir todos
oauth-client=Cliente
oauth-client.token.issue.date=Data de emissão
oauth-client.expires.date=Data de vencimento
oauth-client.issued.tokens=Tokens emitidos
client.details.add=Adicionar cliente OAuth
oauth-client.create=Criar cliente OAuth
oauth-client.id=ID do cliente
oauth-client.secret=Segredo do cliente
oauth-client.redirect.uri=URI de redirecionamento do cliente
oauth-client.access-token.accessTokenValiditySeconds=Acessar validade do token
oauth-client.access-token.refreshTokenValiditySeconds=Atualizar validade do token
oauth-client.access-token.defaultDuration=Usar padrão
oauth-client.title=Título do cliente
oauth-client.description=Descrição
oauth2.error.invalid_client=ID de cliente inválido. Valide o id e parâmetros secretos do cliente.

team.user.enter.email=Digite o email do usuário
user.not.found=Usuário não encontrado
team.profile.update.title=Atualizar as informações da equipe

autocomplete.genus=Encontrar Genus

stats.number-of-countries={0} Países
stats.number-of-institutes={0} Institutos
stats.number-of-accessions={0} Accessões

navigate.back=\\u21E0 Back


content.page.list.title=Lista de artigos
article.lang=Idioma
article.classPk=Objeto

session.expiry-warning-title=Aviso de expiração da sessão
session.expiry-warning=Sua sessão atual está prestes a expirar. Deseja estender esta sessão?
session.expiry-extend=Estender sessão

download.kml=Fazer download de KML

data-overview=Visão geral estatística
data-overview.short=Visão geral
data-overview.institutes=Institutos depositários
data-overview.composition=Composição dos bancos genéticos depositários
data-overview.sources=Fontes de material
data-overview.management=Gerenciamento da coleta
data-overview.availability=Disponibilidade do material
data-overview.otherCount=Outros
data-overview.missingCount=Não especificado
data-overview.totalCount=Total
data-overview.donorCode=FAO WIEWS código do instituto doador
data-overview.mlsStatus=Disponível para distribuição sob o MLS
