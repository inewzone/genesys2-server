package org.genesys2.server.cache;

import java.io.Serializable;

import org.springframework.cache.Cache;
import org.springframework.security.acls.domain.AclAuthorizationStrategy;
import org.springframework.security.acls.domain.AclImpl;
import org.springframework.security.acls.model.AclCache;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.PermissionGrantingStrategy;
import org.springframework.security.util.FieldUtils;
import org.springframework.util.Assert;

public class HazelcastAclCache implements AclCache {
	// ~ Instance fields
	// ================================================================================================

	private PermissionGrantingStrategy permissionGrantingStrategy;
	private AclAuthorizationStrategy aclAuthorizationStrategy;
	private final Cache cache;

	// ~ Constructors
	// ===================================================================================================

	public HazelcastAclCache(Cache cache, PermissionGrantingStrategy permissionGrantingStrategy, AclAuthorizationStrategy aclAuthorizationStrategy) {
		Assert.notNull(cache, "Cache required");
		Assert.notNull(permissionGrantingStrategy, "PermissionGrantingStrategy required");
		Assert.notNull(aclAuthorizationStrategy, "AclAuthorizationStrategy required");
		this.cache = cache;
		this.permissionGrantingStrategy = permissionGrantingStrategy;
		this.aclAuthorizationStrategy = aclAuthorizationStrategy;
	}

	// ~ Methods
	// ========================================================================================================

	@Override
	public void evictFromCache(Serializable pk) {
		Assert.notNull(pk, "Primary key (identifier) required");

		final MutableAcl acl = getFromCache(pk);

		if (acl != null) {
			cache.evict(acl.getId());
			cache.evict(acl.getObjectIdentity());
		}
	}

	@Override
	public void evictFromCache(ObjectIdentity objectIdentity) {
		Assert.notNull(objectIdentity, "ObjectIdentity required");

		final MutableAcl acl = getFromCache(objectIdentity);

		if (acl != null) {
			cache.evict(acl.getId());
			cache.evict(acl.getObjectIdentity());
		}
	}

	@Override
	public MutableAcl getFromCache(ObjectIdentity objectIdentity) {
		Assert.notNull(objectIdentity, "ObjectIdentity required");

		MutableAcl element = null;

		try {
			element = (MutableAcl) cache.get(objectIdentity);
		} catch (final Throwable ignored) {
		}

		if (element == null) {
			return null;
		}
		return initializeTransientFields(element);
	}

	@Override
	public MutableAcl getFromCache(Serializable pk) {
		Assert.notNull(pk, "Primary key (identifier) required");

		MutableAcl element = null;

		try {
			element = (MutableAcl) cache.get(pk);
		} catch (final Throwable ignored) {
		}

		if (element == null) {
			return null;
		}
		return initializeTransientFields(element);
	}

	@Override
	public void putInCache(MutableAcl acl) {
		Assert.notNull(acl, "Acl required");
		Assert.notNull(acl.getObjectIdentity(), "ObjectIdentity required");
		Assert.notNull(acl.getId(), "ID required");

		if (this.aclAuthorizationStrategy == null) {
			if (acl instanceof AclImpl) {
				this.aclAuthorizationStrategy = (AclAuthorizationStrategy) FieldUtils.getProtectedFieldValue("aclAuthorizationStrategy", acl);
				this.permissionGrantingStrategy = (PermissionGrantingStrategy) FieldUtils.getProtectedFieldValue("permissionGrantingStrategy", acl);
			}
		}

		if (acl.getParentAcl() != null && acl.getParentAcl() instanceof MutableAcl) {
			putInCache((MutableAcl) acl.getParentAcl());
		}

		cache.put(acl.getObjectIdentity(), acl);
		cache.put(acl.getId(), acl);
	}

	private MutableAcl initializeTransientFields(MutableAcl value) {
		if (value instanceof AclImpl) {
			FieldUtils.setProtectedFieldValue("aclAuthorizationStrategy", value, this.aclAuthorizationStrategy);
			FieldUtils.setProtectedFieldValue("permissionGrantingStrategy", value, this.permissionGrantingStrategy);
		}

		if (value.getParentAcl() != null) {
			initializeTransientFields((MutableAcl) value.getParentAcl());
		}
		return value;
	}

	@Override
	public void clearCache() {
		cache.clear();
	}
}
