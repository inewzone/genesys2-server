/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import org.genesys2.server.exception.AuthorizationException;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Controller("restOrganizationController")
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/org", "/json/v0/org" })
public class OrganizationController extends RestController {

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private BatchRESTService batchRESTService;

	/**
	 * List organizations
	 *
	 * @return
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	List<Organization> listOrganizations(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		return organizationService.list(new PageRequest(page - 1, 50)).getContent();
	}

	/**
	 * Create or update organization
	 *
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "", method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object updateOrganization(@RequestBody Organization organizationJson) throws ValidationException {
		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(organizationJson);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation failed", violations);
		}
		Organization organization = organizationService.getOrganization(organizationJson.getSlug());

		if (organization == null) {
			organization = organizationService.create(organizationJson.getSlug(), organizationJson.getTitle());
		}
		return organizationService.update(organization.getId(), organization.getSlug(), organizationJson.getTitle());
	}

	/**
	 * Get organization details
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Organization getOrganization(@PathVariable("shortName") String shortName) throws AuthorizationException {
		return organizationService.getOrganization(shortName);
	}

	/**
	 * Get organization blurp
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}/blurp/{language}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	String getBlurp(@PathVariable("shortName") String shortName, @PathVariable("language") String language) throws AuthorizationException {
		Organization org = organizationService.getOrganization(shortName);
		Article article = organizationService.getBlurp(org, new Locale(language));
		return article == null ? null : article.getBody();
	}

	/**
	 * Update blurp
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}/blurp", method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Article updateBlurp(@PathVariable("shortName") String shortName, @RequestBody OrganizationBlurpJson blurp) throws AuthorizationException {
		Organization org = organizationService.getOrganization(shortName);
		return organizationService.updateBlurp(org, blurp.blurp, blurp.getLocale());
	}

	/**
	 * Get organization details
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Organization deleteOrganization(@PathVariable("shortName") String shortName) throws AuthorizationException {
		return organizationService.deleteOrganization(organizationService.getOrganization(shortName));
	}

	/**
	 * Get organization details
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}/institutes", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Set<String> getOrganizationMembers(@PathVariable("shortName") String shortName) throws AuthorizationException {
		Set<String> instCodes = new HashSet<String>();
		for (FaoInstitute inst : organizationService.getMembers(organizationService.getOrganization(shortName))) {
			instCodes.add(inst.getCode());
		}
		return instCodes;
	}

	/**
	 * Add Institutes to Organization
	 *
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/{slug}/add-institutes", method = { RequestMethod.POST, RequestMethod.PUT }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	boolean addOrganizationInstitutes(@PathVariable(value = "slug") String slug, @RequestBody List<String> instituteList) throws JsonProcessingException,
			IOException {
		// TODO Check user's permissions to update this organization.
		final Organization organization = organizationService.getOrganization(slug);
		if (organization == null) {
			throw new ResourceNotFoundException();
		}

		return organizationService.addOrganizationInstitutes(organization, instituteList);
	}

	/**
	 * Set Institutes of Organization
	 *
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/{slug}/set-institutes", method = { RequestMethod.POST, RequestMethod.PUT }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	boolean setOrganizationInstitutes(@PathVariable(value = "slug") String slug, @RequestBody List<String> instituteList) throws JsonProcessingException,
			IOException {
		// TODO Check user's permissions to update this organization.
		final Organization organization = organizationService.getOrganization(slug);
		if (organization == null) {
			throw new ResourceNotFoundException();
		}

		return organizationService.setOrganizationInstitutes(organization, instituteList);
	}

	public static class OrganizationBlurpJson {
		public String blurp;
		public String locale;

		public Locale getLocale() {
			return locale == null ? null : new Locale(locale);
		}
	}
}
