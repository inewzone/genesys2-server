package org.genesys2.server.servlet.controller;

import java.security.Principal;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.genesys2.server.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/download")
public class DownloadController extends BaseController {
	@Autowired
	private ContentService contentService;

	@RequestMapping(value = "/**", method = RequestMethod.POST)
	public String downloadInf(ModelMap model, HttpServletRequest request, Locale locale, Principal principal, @RequestParam Map<String, String> query) {
		_logger.info("Downloader " + request.getServletPath());

		if (principal instanceof UsernamePasswordAuthenticationToken) {
			_logger.info("Showing download screen for authenticated users");
			model.addAttribute("info", contentService.getGlobalArticle("download-authenticated", locale));
		} else {
			_logger.info("Showing download screen for anonymous users");
			model.addAttribute("info", contentService.getGlobalArticle("download-anonymous", locale));
		}
		model.addAttribute("path", request.getServletPath().substring("/download".length()));
		query.remove("_csrf");
		model.addAttribute("query", query);
		_logger.info("Ready for download");
		return "/download/index";
	}
}
