/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rdf;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TraitService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/acn", headers = "accept=text/turtle", produces = "text/turtle")
public class AccessionControllerRdf {

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private CropService cropService;
	
	@Autowired
	private TraitService traitService;
	

	@RequestMapping(value = "/id/{accessionId}", method = RequestMethod.GET)
	public String viewTurtle(ModelMap model, @PathVariable(value = "accessionId") long accessionId) {
		final Accession accession = genesysService.getAccession(accessionId);
		if (accession == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("accession", accession);
		model.addAttribute("accessionNames", genesysService.listAccessionNames(accession.getAccessionId()));
		model.addAttribute("accessionExchange", genesysService.listAccessionExchange(accession.getAccessionId()));
		model.addAttribute("accessionCollect", genesysService.listAccessionCollect(accession.getAccessionId()));
		model.addAttribute("accessionBreeding", genesysService.listAccessionBreeding(accession.getAccessionId()));
		model.addAttribute("accessionGeo", genesysService.listAccessionGeo(accession.getAccessionId()));

		model.addAttribute("metadatas", genesysService.listMetadata(accession.getAccessionId()));
		model.addAttribute("methods", traitService.listMethods(accession.getAccessionId()));
		model.addAttribute("methodValues", genesysService.getAccessionTraitValues(accession.getAccessionId()));

		model.addAttribute("crops", cropService.getCrops(accession.getTaxonomy()));

		return "/accession/details-turtle";
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public void downloadFiltered(@RequestParam(value = "filter", required = false, defaultValue = "{}") String jsonFilter, HttpServletResponse response)
			throws IOException {
		final OutputStream outputStream = response.getOutputStream();
		
		// TODO
		
		
		response.flushBuffer();
	}

}
