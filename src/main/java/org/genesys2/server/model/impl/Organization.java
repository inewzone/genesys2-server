/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.genesys2.server.model.AuditedModel;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "organization")
public class Organization extends AuditedModel {

	private static final long serialVersionUID = 2710908645431936666L;

	@Column(nullable = false, length = 150)
	private String slug;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String title;

	@JsonIgnore
	@ManyToMany(cascade = {}, fetch = FetchType.LAZY, targetEntity = FaoInstitute.class)
	@JoinTable(name = "organizationinstitute", joinColumns = @JoinColumn(name = "organizationId"), inverseJoinColumns = @JoinColumn(name = "instituteId"))
	@OrderBy("code")
	private List<FaoInstitute> members = new ArrayList<FaoInstitute>();

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<FaoInstitute> getMembers() {
		return members;
	}

	public void setMembers(List<FaoInstitute> members) {
		this.members = members;
	}
}
