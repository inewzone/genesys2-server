/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.genesys2.server.model.BusinessModel;
import org.hibernate.annotations.Type;

/**
 * Accession "alias"
 */
@Entity
@Table(name = "accessionremark")
public class AccessionRemark extends BusinessModel implements AccessionRelated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 815335916194920054L;

	@Version
	private long version = 0;

	@ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {})
	@JoinColumn(name = "accessionId", nullable = false, updatable = false)
	private AccessionId accession;

	@Lob
	@Column(name = "remark")
	@Type(type = "org.hibernate.type.TextType")
	private String remark;

	@Column(length = 30)
	private String fieldName;

	public AccessionRemark() {
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public AccessionId getAccession() {
		return accession;
	}

	public void setAccession(AccessionId accession) {
		this.accession = accession;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

}
