/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.kpi;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.model.EntityId;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Holds results of {@link Execution} run.
 */
@Entity
@Table(name = "kpiobservation")
public class Observation implements EntityId, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7621192124417368090L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@Column(name = "`value`")
	private double value;

	@JsonIgnore
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "executionRunId")
	private ExecutionRun executionRun;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "kpiobservationdimension", joinColumns = @JoinColumn(name = "observationId"), inverseJoinColumns = @JoinColumn(name = "dimensionKeyId"))
	private Set<DimensionKey> dimensions = new HashSet<DimensionKey>();

	@Column
	private int dimensionCount;

	@PrePersist
	void prePersist() {
		this.dimensionCount = this.dimensions == null ? 0 : this.dimensions.size();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Set<DimensionKey> getDimensions() {
		return dimensions;
	}

	public void setDimensions(Set<DimensionKey> dimensions) {
		this.dimensions = dimensions;
	}

	public int getDimensionCount() {
		return this.dimensionCount;
	}

	public void setDimensionCount(int dimensionCount) {
		this.dimensionCount = dimensionCount;
	}

	public ExecutionRun getExecutionRun() {
		return executionRun;
	}

	public void setExecutionRun(ExecutionRun executionRun) {
		this.executionRun = executionRun;
	}

	@Override
	public String toString() {
		return "value=" + value + " D=" + dimensions;
	}

	public boolean hasDimensionKeys(Set<DimensionKey> otherDks) {
		if (getDimensions().containsAll(otherDks) && otherDks.containsAll(getDimensions())) {
			return true;
		}
		return false;
	}
}
