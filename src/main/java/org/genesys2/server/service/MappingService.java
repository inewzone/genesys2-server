/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.IOException;

import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;

public interface MappingService {

	void clearCache();

	String filteredKml(AppliedFilters filters);

	String filteredGeoJson(AppliedFilters filters, Integer limit) throws IOException;

	byte[] getTile(AppliedFilters filters, int zoom, int xtile, int ytile);

	public static class CoordUtil {
		public static double tileToLon(int zoom, int xtile) {
			return Math.floor((double) xtile / (1 << zoom) * 360.0 - 180.0);
		}

		public static double tileToLat(int zoom, int ytile) {
			final double n = Math.PI - 2.0 * Math.PI * ytile / (1 << zoom);
			return Math.toDegrees(Math.atan(Math.sinh(n)));
		}

		public static int tileToLon1(int zoom, int xtile) {
			// n = 2 ^ zoom
			final double n = 1 << zoom;
			// lon_deg = xtile / n * 360.0 - 180.0
			final double lon = xtile / n * 360.0 - 180.0;
			return (int) Math.floor(lon);
		}

		public static int tileToLat1(int zoom, int ytile) {
			// n = 2 ^ zoom
			final double n = 1 << zoom;
			// lat_rad = arctan(sinh(π * (1 - 2 * ytile / n)))
			final double latr = Math.atan(Math.sinh(Math.PI * (1.0d - 2.0d * ytile / n)));
			// lat_deg = lat_rad * 180.0 / π
			final double lat = latr * 180.0 / Math.PI;
			return (int) Math.floor(lat);
		}

		public static int lonToTile(int zoom, double lon) {
			return (int) Math.floor((lon + 180.0) / 360.0 * (1 << zoom));
		}

		public static int latToTile(int zoom, double lat) {
			final double latr = Math.toRadians(lat);
			return (int) Math.floor((1 - Math.log(Math.tan(latr) + 1 / Math.cos(latr)) / Math.PI) / 2 * (1 << zoom));
		}

		public static int lonToImg(int zoom, double lon) {
			// n = 2 ^ zoom
			final int xtile = lonToTile(zoom, lon);
			final double a = tileToLon(zoom, xtile);
			final double b = tileToLon(zoom, xtile + 1);
			// System.err.println("a=" + a + " b=" + b);

			// Translate to start of tile
			lon -= a;
			// Scale by (ba)*256;
			// System.err.println("b-a="+(b-a));
			lon = 256.0 * lon / (b - a);

			return (int) Math.floor(lon);
		}

		public static int latToImg(int zoom, double lat) {
			// n = 2 ^ zoom
			final int ytile = latToTile(zoom, lat);
			// System.err.println("ytile=" + ytile);
			final double a = tileToLat(zoom, ytile);
			final double b = tileToLat(zoom, ytile + 1);
			// System.err.println("a=" + a + " b=" + b);

			// Translate to start of tile
			lat -= a;
			// Scale by (ba)*256;
			// System.err.println("b-a=" + (b - a));
			lat = 256.0 * lat / (b - a);

			return (int) Math.floor(lat);
		}

		public static int latToImg3(int zoom, int tile, double lat) {
			final double latr = Math.toRadians(lat);
			final double pixAtZoom = (1 << zoom) * 256;
			final double y1 = Math.floor((1 - Math.log(Math.tan(latr) + 1 / Math.cos(latr)) / Math.PI) / 2 * pixAtZoom);
			return (int) y1 - tile * 256;
		}

		public static int lonToImg3(int zoom, int tile, double lng) {
			final double pixAtZoom = (1 << zoom) * 256;
			return (int) (Math.floor((180.0 + lng) / 360.0 * pixAtZoom) - tile * 256);
		}
	}
}
