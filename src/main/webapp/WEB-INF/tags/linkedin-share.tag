<%@ tag description="Share on LinkedIn!" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="url" required="false" type="java.lang.String" description="The url parameter contains an absolute HTTP or HTTPS URL to be shared on Twitter. The shared URL will be shortened by Twitter’s t.co service in a published Tweet. A Twitter Card may be appear for a shared URL." %>
<%@ attribute name="text" required="false" type="java.lang.String" description="A text parameter appears pre-selected in a Tweet composer. The Tweet author may easily remove the text with a single delete action." %>
<%@ attribute name="summary" required="false" type="java.lang.String" description="The url-encoded description that you wish you use" %>
<%@ attribute name="dataSize" required="false" type="java.lang.String" description="regular or large" %>

<%--
  https://developer.linkedin.com/docs/share-on-linkedin
--%>

<c:if test="${url eq null}">
  <c:set var="url" value="${props.baseUrl}${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}" />
</c:if>

<c:url var="refUrl" value="${url}" />

<c:url var="shareUrl" value="https://www.linkedin.com/shareArticle">
  <c:param name="mini" value="true" />
  <c:param name="source" value="GenesysPGR" />
  <c:param name="url" value="${refUrl}" />
  <c:if test="${text != null and text ne ''}">
    <c:param name="title" value="${jspHelper.htmlToText(text, 200)}" />
  </c:if>
  <c:if test="${summary != null and summary ne ''}">
    <c:param name="summary" value="${jspHelper.htmlToText(summary, 256)}" />
  </c:if>
</c:url>
<a class="linkedin-share-button" target="_blank" href="${shareUrl}" data-size="${dataSize}"><spring:message code="linkedin.share-this" /></a>
