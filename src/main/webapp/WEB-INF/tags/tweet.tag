<%@ tag description="Tweet this!" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="url" required="false" type="java.lang.String" description="The url parameter contains an absolute HTTP or HTTPS URL to be shared on Twitter. The shared URL will be shortened by Twitter’s t.co service in a published Tweet. A Twitter Card may be appear for a shared URL." %>
<%@ attribute name="text" required="false" type="java.lang.String" description="A text parameter appears pre-selected in a Tweet composer. The Tweet author may easily remove the text with a single delete action." %>
<%@ attribute name="hashTags" required="false" type="java.lang.String" description="Add a comma-separated list of hashtags to a Tweet using the hashtags parameter. Omit a preceding “#” from each hashtag; the Tweet composer will automatically add the proper space-separated hashtag by language." %>
<%@ attribute name="dataSize" required="false" type="java.lang.String" description="regular or large" %>

<c:if test="${url eq null}">
  <c:set var="url" value="${props.baseUrl}${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}" />
</c:if>

<c:url var="refUrl" value="${url}" />

<c:url var="twatUrl" value="https://twitter.com/intent/tweet">
  <c:param name="url" value="${refUrl}" />
  <c:if test="${text != null and text ne ''}">
    <c:param name="text" value="${jspHelper.htmlToText(text, 300)}" />
  </c:if>
  <c:if test="${hashTags != null and hashTags ne ''}">
    <c:param name="hashtags" value="${hashTags}" />
  </c:if>
</c:url>
<a class="twitter-share-button" target="_blank" href="${twatUrl}" data-size="${dataSize}"><spring:message code="twitter.tweet-this" /></a>
