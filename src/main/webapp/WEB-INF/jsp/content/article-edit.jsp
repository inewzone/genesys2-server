<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>

    <title>${title}</title>
</head>
<body>
<h1>
    <spring:message code="article.edit-article"/>
</h1>

<c:if test="${article.lang eq 'en'}">
    <div class="form-group">
    	<c:if test="${responseFromTransifex ne null}">
        <div class="alert alert-warning"><spring:message code="${responseFromTransifex}" /></div>
        </c:if>
        
        <form method="post" action="<c:url value="/content/transifex"/>">
            <input id="articleSlug" type="hidden" name="slug" value="${article.slug}"/>
	        <input type="submit" name="post" class="btn btn-default" value="<spring:message code="article.post-to-transifex" />" />
            <input type="submit" name="remove" class="btn btn-default" value="<spring:message code="article.remove-from-transifex" />" />
            <!-- CSRF protection -->
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
    </div>
    <div>${resource}</div>
</c:if>

<c:if test="${article.lang ne 'en'}">
    <div class="form-group">
        <div> ${responseFromTransifex}</div>
        <a href="<c:url value="/content/translate/${article.slug}/${article.lang}"/>" class="btn btn-default">Fetch from
            Transifex</a>
    </div>
</c:if>

<form dir="${article.lang=='fa' || article.lang=='ar' ? 'rtl' : 'ltr'}" role="form" id="editForm" class=""
      action="<c:url value="/content/save-article/${article.lang}" />" method="post">
    <c:if test="${article.id ne null}">
        <input type="hidden" name="id" value="${article.id}"/>
    </c:if>
    <div class="form-group">
        <label for="article-slug" class="control-label"><spring:message code="article.slug"/></label>

        <div class="controls">
            <input type="text" id="article-slug" name="slug" value="<c:out value="${article.slug}" />"
                   class="span9 form-control required"/>
        </div>
    </div>
    <div class="form-group">
        <label for="article-title" class="control-label"><spring:message code="article.title"/></label>

        <div class="controls">
            <input type="text" id="article-title" name="title" value="<c:out value="${article.title}" />"
                   class="span9 form-control required"/>
        </div>
    </div>
    <div dir="${article.lang=='fa' || article.lang=='ar' ? 'rtl' : 'ltr'}" class="form-group">
        <label for="article-body" class="control-label"><spring:message code="article.body"/></label>

        <div class="controls">
            <textarea  id="article-body" name="body" class="span9 required form-control html-editor"><c:out
                    value="${article.body}" escapeXml="false"/></textarea>
        </div>

    </div>

    <input type="submit" value="<spring:message code="save"/>" class="btn btn-primary"/>
    <a href="<c:url value="${article.id ne null ? '/content/'.concat(article.slug) : '/' }" />" class="btn btn-default">Cancel</a>
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

<content tag="javascript">
    <script type="text/javascript">
        jQuery(document).ready(function () {
            if (window.location.hash == '#raw') {
            } else {
              <local:tinyMCE selector=".html-editor" directionality="document.getElementById('editForm').dir" />
            }
        });
    </script>
</content>
</body>
</html>