<%@include file="/WEB-INF/jsp/init.jsp"%>

<div class="free-text blurp" dir="${blurp.lang=='fa' || blurp.lang=='ar' ? 'rtl' : 'ltr'}">
	<c:out value="${blurp.body}" escapeXml="false" />
</div>

<c:if test="${blurp!=null && blurp.lang != pageContext.response.locale.language}">
	<%@include file="/WEB-INF/jsp/not-translated.jsp" %>
</c:if>
